#write the instructions
# stage - 1
FROM openjdk:11 as builder

WORKDIR /app

COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .

RUN chmod +x ./mvnw && ./mvnw -B dependency:go-offline

# Build
COPY src src

RUN ./mvnw clean package -DskipTests
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

# Artifact

FROM openjdk:11.0.13-jre-slim-buster

# create an variable
ARG DEPENDENCY=/app/target/dependency

COPY --from=builder ${DEPENDENCY}/BOOT-INF/classes /app
COPY --from=builder ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=builder ${DEPENDENCY}/META-INF /app/META-INF

EXPOSE 8222

ENTRYPOINT ["java" , "-cp" , "app:app/lib/*", "com.classpath.ordermicroservice.OrderMicroserviceApplication"]








