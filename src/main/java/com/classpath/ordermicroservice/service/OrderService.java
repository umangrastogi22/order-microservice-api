package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;

    private final RestTemplate restTemplate;

    @CircuitBreaker(name = "inventorymicroservice" , fallbackMethod = "fetchOrder")
    public Order saveOrder(Order order){
        log.info("Saving the order {}", order);
        final ResponseEntity<Integer> responseEntity = this.restTemplate.postForEntity("http://localhost:9222/api/v1/inventory", null, Integer.class);
        log.info(" Response from inventory service , {} ", responseEntity.getBody());
        return this.orderRepository.save(order);
    }

    private Order fetchOrder(Exception exception){
        log.error(" Exception while updating the inventory ::  {} ", exception.getMessage());
        return Order.builder().orderId(1111).customerName("ramesh").emailAddress("ramesh@gmail.com").date(LocalDate.now()).build();
    }

    public Set<Order> fetchOrders(){
        return new HashSet<>(this.orderRepository.findAll());
    }

    public Order findById(long orderId){
        return this.orderRepository
                .findById(orderId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid order id"));
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }
}