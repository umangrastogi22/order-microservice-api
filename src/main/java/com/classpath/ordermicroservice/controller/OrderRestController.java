package com.classpath.ordermicroservice.controller;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderRestController {

    private final OrderService orderService;

    @PostMapping
    public Order saveOrder(@RequestBody Order order){
            return this.orderService.saveOrder(order);
    }

    @GetMapping
    public Set<Order> fetchAllOrders(){
        return this.orderService.fetchOrders();
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable long id){
        return this.orderService.findById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteOrderById(@PathVariable long id){
        this.orderService.deleteOrderById(id);
    }
}